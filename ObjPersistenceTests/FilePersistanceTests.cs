﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObjPersistence;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FilePersistence.Tests
{
    [DataContractAttribute]
    class Book
    {
        [DataMemberAttribute] public string name { get; set; }
        [DataMemberAttribute] public string author { get; set; }

        public void print()
        {
            Console.WriteLine($"{name} {author}");
        }
    }

    [TestClass()]
    public class FilePersistanceTests
    {
        [TestMethod()]
        public void TestDict()
        {
            var x = new SortedDictionary<string, string>();
            x.Add("cian", "!23");
            x.Add("carina", "!!!");

            string filePath = @"D:\test.xml";

            FilePersistance.Save<SortedDictionary<string, string>>(x, filePath);

            var loadObj = FilePersistance.Load<SortedDictionary<string, string>>(filePath);

            // loadObj.Select(a => $"{a.Key}: {a.Value}{Environment.NewLine}").ToList().ForEach(Console.WriteLine);
            Assert.IsTrue(loadObj.Count == 2, "Count is: " + loadObj.Count);
            Assert.IsTrue(loadObj["cian"] == "!23", "cian");
            Assert.IsTrue(loadObj["carina"] == "!!!", "carina");
        }

        [TestMethod()]
        public void TestObj()
        {
            Book b1 = new Book() { name = "My book", author = "Ivan" };
            Book b2 = new Book() { name = "My book 2", author = "Ivan" };

            List<Book> l = new List<Book>() { b1, b2 };
            string filePath = @"D:\books.xml";
            FilePersistance.Save<List<Book>>(l, filePath);

            var loadObj = FilePersistance.Load<List<Book>>(filePath);
            // loadObj.ForEach(b => b.print());
            Assert.IsTrue(loadObj.Count == 2, "Count is: " + loadObj.Count);
            Assert.IsTrue(loadObj[0].name == "My book", loadObj[0].name);
        }
    }
}
﻿using System.IO;
using System.Runtime.Serialization;

namespace ObjPersistence
{
    public class FilePersistance
    {
        /// <summary>
        /// Saves an object into a serialized XML format
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="objectToSave">The object to save</param>
        /// <param name="filePath">The file path where to save the object</param>
        public static void Save<T>(T objectToSave, string filePath)
        {
            using FileStream writer = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            ser.WriteObject(writer, objectToSave);
        }

        /// <summary>
        /// Retrieves an object from a serialized XML format
        /// </summary>
        /// <typeparam name="T">The type of the saved object</typeparam>
        /// <param name="filePath">The file from which to read the object</param>
        /// <returns></returns>
        public static T Load<T>(string filePath)
        {
            using FileStream reader = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            return (T)ser.ReadObject(reader);
        }
    }
}
